# Content

This directory contains stuff used on the GitLab CI with tr181-security
manager.

## autocert

Contains pre-generated certificates used during testing.

## generate_testing_certs.sh

Helper script for testing certificate generation.
